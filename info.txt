Teooriaküsimused

  Teooriaküsimustele saate vastata 15 minuti jooksul alates eksami algusest
  (hilisemad vastused ei lähe arvesse).

  Vastused kirjutage siia samasse faili küsimuse alla.
  Iga küsimuse vastus peab jääma alla 200 märgi.
  Iga küsimus annab kuni 4 punkti.

  1. Mis kasu on mallidest veebirakenduses?

  2. Http GET päring ei sobi kustutamise käsu andmiseks. Miks?

  3. Miks on siin vajalik "Host" rida? Kas server ise ei tea kes ta on?
     GET /index.html HTTP/1.1
     Host: enos.itcollege.ee

  4. Kuidas saab serveris asuv veebirakendus kliendi arvutis olevast küpsisest (cookie) info kätte.

Ülesanne 1 (6 punkti)

  Html failide struktuur asub kataloogis "ex1".

  Kirjutage järgmised relatiivsed lingid:

  index.html -> a.html (failis index.html on link, mis viitab failile a.html)
  a.html -> e.html
  e.html -> d.html
  d.html -> e.html
  e.html -> b.html

  Lahenduse kontrollimiseks on ka test ex1/tests.php
  Test eeldab, et failid on saadaval aadressilt http://localhost:8080
  Seega testi kasutamiseks käivitage enne php server (php -S localhost:8080 -t ex1).

Ülesanne 2 (8 punkti)

  Failis ex2/css.html on numbrid 1-5. Muutke faili styles.css nii, et
  täidetud oleks allolevad tingimused.

    • 1 on roheline ja italic
    • 2 on punane ja mitte italic
    • 3 on roheline ja italic
    • 4 on sinine ja italic
    • 5 on sinine ja italic

  Faili css.html muuta ei tohi.

Ülesanne 3 (10 punkti)

  Kataloogis ex3 on rakendus, mis peaks tegema järgmist.
  Rakenduse poole esmakordselt pöördudes näidatakse sisestuse vormi.
  Kui kirjutada vormi mingi sisend ja vajutada "Saada" peaks näidatama lehte,
  mille ainus sisu on see, mis vormi väljale kirjutati tagurpidi. Kirjutage
  puuduvad osad, et rakendus tööle hakkaks.

  Kasuks võibad tulla funktsioonid:
    strrev() - pöörab argument stringi tagrupidi ("abc" -> "cba")
    urlencode() - asendab sümbolid, mida ei ole lubatud url-i kirjutada.

  Infoks: renderTemplate() on meetod, mis tagastab failist loetud malli sisu,
  milles on tehtud mõned asendused. Meetodi esimene argument ütleb, millist
  malli näidata ja teine argument on sõnastik, milles on väärtused mallis
  kasutatavatele muutujatele. Antud ülesandes on mallis üks muutuja ($message).
  Sõne "{{ $message }}" asendatakse sõnastikust saadud võtme "message" väärtusega.

Ülesanne 4 (10 punkti)

  Failis ex4/ex4.php on kood, mis teeb andmebaasi ühenduse ja käivitab failis data.sql
  olevad SQL laused.

  Need SQL laused sisaldavad sisaldavad infot tudengite ja hinnete kohta.

  On tagatud et iga tudengi nimi on unikaalne.

  Teie ülesanne on täita lüngad, nii et programm väljastaks iga tudengi nime kohta,
  mis on selle tudengi hinnete standardhälve.

  Standard hälbe arvuamiseks on failis functions.php funktsioon standardDeviation(),
  mis võtab sisse listi hinnetest ja tagastab nende standardhälbe.

  Kui tudengil ühtegi hinnet pole, siis tema nime ei kuvata. Ka päring ei tohiks
  tagastada tudengeid, kellel ühtegi hinnet ei ole.

  Kui kood on õige peaks vastus olema:
    Alice: 0.47
    Bob: 0

  Andmebaasina kasutatakse mälupõhist andmebaasi ja on võimalik, et teie Php seadistuses
  pole vastavat draiverit automaatselt laetud. Seega peaksite koodi käivitama nii:
    php -d extension=pdo_sqlite ex4.php